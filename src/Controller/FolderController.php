<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Form\FolderType;
use App\Repository\FolderRepository;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class FolderController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private FolderRepository $folderRepository;
    private LinkRepository $linkRepository;
    private PaginatorInterface $paginator;

    public function __construct(
        EntityManagerInterface $entityManager,
        FolderRepository $folderRepository,
        LinkRepository $linkRepository,
        PaginatorInterface $paginator
    ) {
        $this->entityManager = $entityManager;
        $this->folderRepository = $folderRepository;
        $this->linkRepository = $linkRepository;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/f/{title}", name="folder_index")
     * @Template("folder/index.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function index($title, Request $request)
    {
        if ($folder = $this->folderRepository->findOneBy(['title' => $title, 'author' => $this->getUser()])) {
            $markElement = null;
            if (isset($_GET['markElement']) && $_GET['markElement']) {
                $markElement = $_GET['markElement'];
            }
            $elements = array_merge(
                $this->folderRepository->findFolders($title, $this->getUser()),
                $this->linkRepository->findLinksByFolder($folder, $this->getUser(), $markElement)
            );
            $elements = $this->paginator->paginate(
                $elements,
                $request->query->getInt('page', 1),
                30
            );
            $marks = $this->linkRepository->findMarks($folder);
        } else {
            throw new NotFoundHttpException("I don't have such a folder");
        }

        return [
            'title' => $title,
            'elements' => $elements,
            'base_folders' => $this->getBaseFolders($title),
            'marks' => $marks,
        ];
    }

    private function getBaseFolders($title)
    {
        $tmp_title = $title;
        while (Folder::DEFAULT_NAME_FOLDER != $tmp_title) {
            $base_folders[] = $tmp_title;
            if ($folder = $this->folderRepository->base_folder($tmp_title)) {
                $tmp_title = $folder->getTitle();
            }
        }
        $base_folders[] = Folder::DEFAULT_NAME_FOLDER;

        return array_reverse($base_folders);
    }

    /**
     * @Route("/new-folder/{title}", name="folder_new")
     * @IsGranted("ROLE_USER")
     */
    public function newFolder(Request $request, $title)
    {
        $newFolderName = $request->get('new_folder_name');
        $newFolderSequence = $request->get('new_folder_sequence');

        if ($request->isMethod('POST') && isset($newFolderName) && !empty($newFolderName)) {
            if ($this->entityManager->getRepository(Folder::class)->findOneBy(['title' => $newFolderName])) {
                throw new \Exception('Folder exists');
            }

            $folder = (new Folder())
                ->setTitle($newFolderName)
                ->setAuthor($this->getUser())
                ->setCreatedAt(new \DateTime())
                ->setSequence($newFolderSequence)
                ->addBaseFolder($this->folderRepository->findOneBy(['title' => $title]));

            $this->entityManager->persist($folder);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('folder_index', ['title' => $title]);
    }

    /**
     * @Route("/remove-folder/{folder}", name="folder_remove")
     * @IsGranted("ROLE_USER")
     */
    public function remove(Folder $folder)
    {
        if (!$folder && Folder::DEFAULT_NAME_FOLDER === $folder->getTitle()) {
            throw new NotFoundHttpException();
        }

        foreach ($folder->getLinks() as $link) {
            $this->entityManager->remove($link);
        }
        foreach ($folder->getFolders() as $item) {
            $this->entityManager->remove($item);
        }

        $folder
            ->removeBaseFolder($folder)
            ->removeFolder($folder);

        $this->entityManager->remove($folder);
        $this->entityManager->flush();

        return $this->redirectToRoute('folder_index', ['title' => Folder::DEFAULT_NAME_FOLDER]);
    }

    /**
     * @Route("/{title}/{folder}/edit-folder", name="folder_edit")
     * @Template("folder/edit.html.twig")
     * @IsGranted("ROLE_USER")
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function edit(Folder $folder, Request $request, $title)
    {
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($folder);
            $this->entityManager->flush();

            return $this->redirectToRoute('folder_index', ['title' => $folder->getTitle()]);
        }

        return [
            'folderForm' => $form->createView(),
            'title' => $title,
            'folder' => $folder,
            'folders' => $this->entityManager->getRepository(Folder::class)->findAll(),
        ];
    }

    /**
     * @Route("/move-folder/{folder}/{folderTo}", name="move_folder")
     * @IsGranted("ROLE_USER")
     */
    public function moveFolder(Folder $folder, Folder $folderTo)
    {
        if (!$folder && !$folderTo) {
            throw new \Exception('Folder missing');
        }

        foreach ($folder->getBaseFolder() as $item) {
            $item->removeFolder($folder);
        }
        $folder->addBaseFolder($folderTo);
        $this->entityManager->flush();

        return $this->redirectToRoute('folder_index', ['title' => $folder->getTitle()]);
    }
}
