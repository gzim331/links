<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Link;
use App\Form\LinkType;
use App\Repository\FolderRepository;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LinkController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private FolderRepository $folderRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        FolderRepository       $folderRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->folderRepository = $folderRepository;
    }

    /**
     * @Route("/{folder}/new-link", name="link_new")
     * @IsGranted("ROLE_USER")
     */
    public function newLink(Request $request, $folder)
    {
        $url = $request->get('new_link_url');
        $title = $request->get('new_link_title');
        $sequence = $request->get('new_link_sequence');
        $img = $request->get('new_link_img');
        $mark = $request->get('new_link_mark');

        if ($request->isMethod('POST')) {
            $sequence = ($sequence !== "") ? $sequence : null;
            $img = ($img !== "") ? $img : null;
            $mark = ($mark !== "") ? $mark : null;
            $url = ($url !== "") ? $url : null;
            $title = ($title !== "") ? $title : null;

            if ($url) {
                if (empty($title)) {
                    $title = $this->getTitleFromUrl($url) ?? $url;
                }
                $link = (new Link())
                    ->setUrl($url)
                    ->setTitle($title)
                    ->setAuthor($this->getUser())
                    ->setCreatedAt(new \DateTime())
                    ->setWebsite(ltrim(parse_url($url, PHP_URL_HOST), 'www.'))
                    ->setSequence($sequence)
                    ->setImg($img)
                    ->setMark($mark)
                    ->setFolder($this->folderRepository->findOneBy(['title' => $folder]));
                $this->entityManager->persist($link);
                $this->entityManager->flush();
            } elseif ($title) {
                $link = (new Link())
                    ->setTitle($title)
                    ->setAuthor($this->getUser())
                    ->setCreatedAt(new \DateTime())
                    ->setSequence($sequence)
                    ->setImg($img)
                    ->setMark($mark)
                    ->setFolder($this->folderRepository->findOneBy(['title' => $folder]));
                $this->entityManager->persist($link);
                $this->entityManager->flush();
            } else {
                throw new BadRequestException();
            }
        }


        return $this->redirectToRoute('folder_index', ['title' => $folder]);
    }

    /**
     * @Route("/{id}/edit-link", name="link_edit")
     * @Template("link/edit.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function edit($id, Request $request)
    {
        $link = $this->entityManager->find(Link::class, $id);
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $url = $form->getData()->getUrl();
            $website = ltrim(parse_url($url, PHP_URL_HOST), 'www.');
            $link->setWebsite($website);
            $this->entityManager->flush();

            return $this->redirectToRoute('folder_index', ['title' => $link->getFolder()->getTitle()]);
        }

        return [
            'linkForm' => $form->createView(),
            'link' => $link,
            'folders' => $this->entityManager->getRepository(Folder::class)->findAll(),
        ];
    }

    /**
     * @Route("/{link}/remove-link", name="link_remove")
     * @IsGranted("ROLE_USER")
     */
    public function remove(Link $link)
    {
        if (!$link) {
            throw new NotFoundHttpException();
        }
        $this->entityManager->remove($link);
        $this->entityManager->flush();

        return $this->redirectToRoute('folder_index', ['title' => $link->getFolder()->getTitle()]);
    }

    private function getTitleFromUrl(string $url)
    {
        if (preg_match('/<title>(.+)<\/title>/', file_get_contents($url), $matches) && isset($matches[1])) {
            return $matches[1];
        }

        return null;
    }

    /**
     * @Route("/move-element/{link}/{folder}", name="move_element")
     * @IsGranted("ROLE_USER")
     */
    public function moveElement(Link $link, Folder $folder)
    {
        if (!$link && !$folder) {
            throw new \Exception('Link or folder missing');
        }

        $link->setFolder($folder);
        $this->entityManager->flush();

        return $this->redirectToRoute('folder_index', ['title' => $folder->getTitle()]);
    }

    /**
     * @Route("/change-pin-link/{link}", name="change_pin_link")
     * @IsGranted("ROLE_USER")
     */
    public function changePinLink(Link $link)
    {
        $link->setPinned(!$link->isPinned());
        $this->entityManager->flush();

        return $this->redirectToRoute('folder_index', ['title' => $link->getFolder()->getTitle()]);
    }
}
