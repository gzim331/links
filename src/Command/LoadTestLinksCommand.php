<?php

namespace App\Command;

use App\Entity\Folder;
use App\Entity\Link;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadTestLinksCommand extends Command
{
    protected static $defaultName = 'load:test:links';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create test links');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Links Creator',
            '- - - - - - -',
            '',
        ]);

        if ($user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $_ENV['DEFAULT_USERNAME']])) {
            if ($baseFolder = $this->entityManager->getRepository(Folder::class)->findOneBy(['author' => $user, 'title' => Folder::DEFAULT_NAME_FOLDER])) {
                for ($c = 1; $c <= 60; $c++) {
                    $link = (new Link())
                        ->setAuthor($user)
                        ->setFolder($baseFolder)
                        ->setSequence($c)
                        ->setTitle('link ' . $c)
                        ->setCreatedAt(new \DateTime())
                    ;
                    $this->entityManager->persist($link);
                }

                $this->entityManager->flush();
            }
        }

        $output->writeln([
            'User admin was created'
        ]);

        return 0;
    }
}
