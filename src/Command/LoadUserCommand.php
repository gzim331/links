<?php

namespace App\Command;

use App\Entity\Folder;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoadUserCommand extends Command
{
    protected static $defaultName = 'load:user';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(string $name = null, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create user admin')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'User Creator',
            '- - - - - - -',
            '',
        ]);

        $plain_username = $_ENV['DEFAULT_USERNAME'];
        $plain_password = $_ENV['DEFAULT_PASSWORD'];

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $plain_username]);
        if ($user) {
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $plain_password
            ));
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } else {
            $user = new User();
            $user->setUsername($plain_username)
                ->setRoles(['ROLE_USER']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $plain_password
            ));
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $folder = new Folder();
            $folder->setTitle(Folder::DEFAULT_NAME_FOLDER)
                ->setCreatedAt(new \DateTime())
//                ->addBaseFolder($folder)
                ->setAuthor($user);
            $this->entityManager->persist($folder);

            $this->entityManager->flush();
        }

        $output->writeln([
            'User admin was created'
        ]);

        return 0;
    }
}
