<?php

namespace App\Command;

use App\Entity\Folder;
use App\Entity\Link;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixCommand extends Command
{
    protected static $defaultName = 'run:fixer';

    private EntityManagerInterface $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create test links');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Fixer',
            '- - - - - - -',
            '',
        ]);

        $this->setPinLinks();

        $this->entityManager->flush();

        return Command::SUCCESS;
    }

    private function setPinLinks(): void
    {
        foreach ($this->entityManager->getRepository(Link::class)->findBy(['pinned' => null]) as $link) {
            $link->setPinned(false);
        }
    }
}
