<?php

namespace App\Command;

use App\Entity\Link;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadWebsitesCommand extends Command
{
    protected static $defaultName = 'load:websites';
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('load websites')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'load websites',
            '- - - - - - -',
            '',
        ]);

        $links = $this->entityManager->getRepository(Link::class)->findAll();
        foreach ($links as $link) {
            $url = $link->getUrl();
            $website = ltrim(parse_url($url, PHP_URL_HOST), 'www.');
            $link->setWebsite($website);
            $this->entityManager->persist($link);
        }
        $this->entityManager->flush();

        $output->writeln([
            'websites was load',
        ]);

        return 0;
    }
}
