<?php

namespace App\Repository;

use App\Entity\Link;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Link|null find($id, $lockMode = null, $lockVersion = null)
 * @method Link|null findOneBy(array $criteria, array $orderBy = null)
 * @method Link[]    findAll()
 * @method Link[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    public function findMarks($folder)
    {
        return $this->createQueryBuilder('link')
            ->select('count(link.mark) as ctn, link.mark')
            ->andWhere('link.folder = :folder')
            ->setParameter('folder', $folder)
            ->andWhere('link.mark IS NOT NULL')
            ->andWhere('link.mark != :value')
            ->setParameter('value', '')
            ->addGroupBy('link.mark')
            ->orderBy('link.mark', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findLinksByFolder($folder, $author, $markElement)
    {
        $elements = $this->createQueryBuilder('link')
            ->addSelect('CASE WHEN link.sequence IS NULL THEN 1 ELSE 0 END as HIDDEN null_sequence')
            ->andWhere('link.folder = :folder')
            ->setParameter('folder', $folder)
            ->andWhere('link.author = :author')
            ->setParameter('author', $author)
            ->addOrderBy('null_sequence', 'ASC')
            ->addOrderBy('link.sequence', 'ASC')
            ->addOrderBy('link.createdAt', 'ASC');

        if (null !== $markElement) {
            $elements->andWhere('link.mark = :value')
                ->setParameter('value', $markElement);
        }

        return $elements->getQuery()->getResult();
    }
}
