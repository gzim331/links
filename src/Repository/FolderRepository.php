<?php

namespace App\Repository;

use App\Entity\Folder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Folder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Folder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Folder[]    findAll()
 * @method Folder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Folder::class);
    }

    public function findFolders($folder, $user)
    {
        return $this->createQueryBuilder('folder')
            ->addSelect('CASE WHEN folder.sequence IS NULL THEN 1 ELSE 0 END as HIDDEN null_sequence')
            ->leftJoin('folder.base_folder', 'base_folder')
            ->andWhere('base_folder.title = :title')
            ->setParameter('title', $folder)
            ->andWhere('folder.author = :author')
            ->setParameter('author', $user)
            ->addOrderBy('null_sequence', 'ASC')
            ->addOrderBy('folder.sequence', 'ASC')
            ->addOrderBy('folder.title', 'ASC')
            ->getQuery()->getResult();
    }

    public function base_folder(string $folder)
    {
        return $this->createQueryBuilder('folder')
            ->leftJoin('folder.folders', 'base_folder')
            ->andWhere('base_folder.title = :folder')
            ->setParameter('folder', $folder)
            ->getQuery()->getOneOrNullResult();
    }
}
