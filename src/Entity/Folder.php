<?php

namespace App\Entity;

use App\Repository\FolderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FolderRepository::class)
 */
class Folder
{
    const DEFAULT_NAME_FOLDER = 'default';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=Folder::class, inversedBy="folders")
     */
    private $base_folder;

    /**
     * @ORM\ManyToMany(targetEntity=Folder::class, mappedBy="base_folder")
     */
    private $folders;

    /**
     * @ORM\OneToMany(targetEntity=Link::class, mappedBy="folder")
     */
    private $links;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="folders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sequence;

    public function __construct()
    {
        $this->base_folder = new ArrayCollection();
        $this->folders = new ArrayCollection();
        $this->links = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getBaseFolder(): Collection
    {
        return $this->base_folder;
    }

    public function addBaseFolder(self $baseFolder): self
    {
        if (!$this->base_folder->contains($baseFolder)) {
            $this->base_folder[] = $baseFolder;
        }

        return $this;
    }

    public function removeBaseFolder(self $baseFolder): self
    {
        if ($this->base_folder->contains($baseFolder)) {
            $this->base_folder->removeElement($baseFolder);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(self $folder): self
    {
        if (!$this->folders->contains($folder)) {
            $this->folders[] = $folder;
            $folder->addBaseFolder($this);
        }

        return $this;
    }

    public function removeFolder(self $folder): self
    {
        if ($this->folders->contains($folder)) {
            $this->folders->removeElement($folder);
            $folder->removeBaseFolder($this);
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setFolder($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
            // set the owning side to null (unless already changed)
            if ($link->getFolder() === $this) {
                $link->setFolder(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSequence()
    {
        return $this->sequence;
    }

    public function setSequence($sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }
}
